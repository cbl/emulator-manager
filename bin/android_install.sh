sudo apt update
sudo apt-get install -y git
sudo apt-get install curl
sudo apt-get install -y ffmpeg

# java8
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get install oracle-java8-installer

# android studio
wget android-studio-ide-181.5056338-linux.zip -O ~/android-studio.zip
unzip ~/android-studio.zip -d ~/
~/Android/studio.sh

# v4l2loopback
wget https://github.com/umlaeute/v4l2loopback/archive/master.zip -O ~/v4l2loopback.zip
unzip ~/v4l2loopback.zip -d ~/
cd ~/v4l2loopback-master
make && sudo make install
sudo depmod -a
cd ~/

# nodejs & npm
curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
sudo apt-get install -y nodejs
sudo ln -s /usr/bin/nodejs /usr/bin/node
sudo apt install -y npm

# emulator manager
git clone https://gitlab.com/cbl/emulator-manager.git ~/
cd ~/emulator-manager
npm install
cd ~/

echo 'export ANDROID_HOME=~/Android/Sdk' >> ~/.bashrc
echo 'export PATH=${PATH}:~/emulator-manager/bin' >> ~/.bashrc
echo 'export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools' >> ~/bashrc
