
const os = require('os')
const util = require('util')
const ChildProcess = require('child_process')
const exec = util.promisify(ChildProcess.exec)

console.log(`Setup on ${os.platform()}.`)

let main = async () => {
    if(os.platform() == 'darwin') {

        console.log("install ffmpeg")
        const process = ChildProcess.spawn('git clone https://github.com/umlaeute/v4l2loopback.git', {shell:true})
        process.stdout.on('data', (data) => {
            console.log('stdout: '+data)
        })
        process.stderr.on('data', (data) => {
            console.log('strerr: '+data)
        })
        process.on('error', (data) => {
            console.log(data)
        })

    } else if(os.platform() == 'linux') {

    }
}

main()

class Installer {
    construct(commands) {
        this.commands = commands
    }

}
