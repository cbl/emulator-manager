import {app, BrowserWindow} from 'electron'
import path from 'path'
import { format as formatUrl } from 'url'

let mainWindow

const isDev = process.env.NODE_ENV !== 'production'

function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 800,
        height: 600
    })

    if(isDev) {
        mainWindow.loadURL(`http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}`)
        mainWindow.webContents.openDevTools()
    } else {
        window.loadUrl(formatUrl({
            pathname: path.join('__dirname', 'index.html'),
            protocol: 'file',
            slashes: 'true'
        }))
    }


    mainWindow.on('closed', function() {
        mainWindow = null
    })
}
app.on('ready', createWindow)

app.on('window-all-closed', function() {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', function() {
    if (mainWindow === null) {
        createWindow()
    }
})
