require('./bootstrap');

import Vue from 'vue'

import 'bootstrap';

import App from './App'
import router from '@/router'
import store from '@/store'

import ApiService from '@/common/api.service'
import ComponentService from '@/common/component.service'

ApiService.init()
ComponentService.init()

new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: {
        App
    }
});
