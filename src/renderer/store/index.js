import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import emulators from './emulators.module'

export default new Vuex.Store({
    modules: {
        emulators
    }
})
