
import Terminal from '@/emulator/terminal'
import Emulator from '@/emulator/emulator'
import Router from '@/router'
import Opn from 'opn'

const initialState = {
    emulator: null,
    emulators: [],
    processes: [],
}

const getters = {
    emulators(state) {
        return state.emulators
    },
    emulator(state) {
        return state.emulator
    },
    processes(state) {
        return state.processes
    }
}

export const state = Object.assign({}, initialState)

export const actions = {
    async ['fetch_emulators']({ commit, dispatch }) {
        let emulators = await Terminal.emulatorsList()
        for(let id in emulators) {
            emulators[id] = new Emulator(emulators[id], id++)
        }
        commit('set_emulators', emulators)
        //dispatch('get_running_emulators')
    },
    async ['create_emulator']({ commit, dispatch }, name) {
        let error = await Terminal.createEmulator(name)
        dispatch('fetch_emulators')
        return error
    },
    async ['rename_emulator']({ commit, dispatch }, {name, new_name}) {
        await Terminal.renameEmulator(name, new_name)
        commit('rename_emulator', {name, new_name})
    },
    async ['delete_emulator']({ commit, dispatch }, emulator) {
        await Terminal.deleteEmulator(emulator.name)
        dispatch('fetch_emulators')
    },
    async ['stop_emulator']({ commit, dispatch }, emulator) {
        await emulator.stop()
    },
    async ['stop_all_emulators']({ commit, dispatch }) {
        await Terminal.stopAll()
    },
    async ['install_apk']({ commit, dispatch }, emulator) {
        if(!emulator.isRunning())
            await dispatch('start_emulator_normal', emulator)
        emulator.installWhatsapp()
    },
    async ['install_apk_vpn']({ commit, dispatch }, emulator) {
        if(!emulator.isRunning())
            await dispatch('start_emulator_normal', emulator)
        emulator.installNordVPN()
    },
    async ['start_emulator_normal']({ commit, dispatch, state }, emulator) {
        if(emulator.isRunning() == 'normal')
            return
        if(emulator.isRunning())
            await emulator.stop()
        await emulator.start('normal')
    },
    async ['start_emulator_headless']({ commit, dispatch, state }, emulator) {
        if(emulator.isRunning() == 'headless')
            return
        if(emulator.isRunning())
            await emulator.stop()
        await emulator.start('headless')
    },
    async ['start_emulator_login']({ commit, dispatch, state }, emulator) {
        if(emulator.isRunning() == 'login')
            return
        if(emulator.isRunning())
            await emulator.stop()
        Opn('http://159.69.198.154/numbers')
        await emulator.start('login')
    },
    async ['get_running_emulators']({ commit, dispatch, state }, emulator) {
        let running_devices = await Terminal.runningDevices()
        commit('set_running_devices', running_devices)
    }
}

export const mutations = {
    ['set_running_devices'](state, devices) {
        for(let i=0;i<state.emulators.length;i++) {
            let emulator = state.emulators[i]
            if(devices.includes(emulator.name)) {
                state.emulators[i].running = true
                state.emulators[i].state = 'running'
            }
        }
    },
    ['set_emulators'](state, emulators) {
        state.emulators = emulators
    },
    ['set_emulator'](state, id) {
        state.emulator = state.emulators.find(x => x.id == id)
    },
    ['set_processes'](state, processes) {
        state.processes = processes
    },
    ['add_process'](state, process) {
        state.processes.push(process)
    },
    ['rename_emulator'](state, {name, new_name}) {
        for(let i=0;i<state.emulators.length;i++) {
            let em = state.emulators[i]
            if(em.name == name)
                state.emulators[i].name= new_name
        }
    }
}

export default {
    state,
    actions,
    mutations,
    getters
}
