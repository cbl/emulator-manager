
import _ from 'lodash'
import util from 'util'
import ChildProcess from 'child_process'
let exec = util.promisify(ChildProcess.exec)
const fixPath = require('fix-path');

fixPath();


const Terminal = {

    optionsString(options) {
        return Object.keys(options).map(function(key){
            return `${key} "${options[key]}"`;
        }).join(" ")
    },

    async isRunningPort(port) {
        let ports = await this.runningDevices()
        return ports.includes(port)
    },

    async runningDevices() {
        const { stdout, stderr } = await exec('adb devices');
        let ports = []
        let lines = stdout.split('\n')
        for(let i=0;i<lines.length;i++) {
            let line = lines[i]
            if(!line.includes('emulator-'))
                continue
            let port = line.split('emulator-')[1].split('\t')[0]
            ports.push(port)
            /*
            let command = `
            TOKEN=$(cat '/Users/lennart/.emulator_console_auth_token')
            expect << EOF
            spawn telnet localhost ${port}
            expect -re ".*>"
            send "auth $TOKEN\r"
            expect -re ".*>"
            send "avd name\r"
            expect -re ".*>"
            send "exit\r"
            EOF
            `
            console.log(line)
            const { stdout, stderr } = await exec(command);
            console.log(stdout)
            console.log(stderr)
            let telnet_lines = stdout.split('\n')
            for(let j=0;j<telnet_lines.length;j++) {
                let telnet_line = telnet_lines[j]
                console.log(telnet_line)
            }
            */
        }
        return ports
    },

    async emulatorsList() {
        const { stdout, stderr } = await exec('emulator -list-avds');
        return _.filter(stdout.split('\n'), (name) => {
            return name != ""
        }).sort()
    },

    async fetchSdks() {
        const { stdout, stderr } = await exec('android list target');
        let lines =  _.filter(stdout.split('\n'), (line) => { return line.startsWith('id:') })
        return lines.map((line) => {
            return {
                id: line.split('id: ')[1].split(' or')[0],
                name: line.split('"')[1].split('"')[0],
            }
        })
    },

    async createEmulator(name) {
        let sdks = await this.fetchSdks()
        let options = this.optionsString({
            '-n': name,
            '-k': `system-images;${sdks[0].name};google_apis;x86`,
            '--device': 'Nexus 5'
        })
        try {
            const { stdout, stderr } = await exec(`echo no | avdmanager create avd ${options}`)
        } catch(e) {
            return e.message.split('\n')[1]
        }
    },

    async deleteEmulator(name) {
        let options = this.optionsString({
            '-n': name,
        })
        const { stdout, stderr } = await exec(`avdmanager delete avd ${options}`)
    },

    async renameEmulator(name, new_name) {
        let options = this.optionsString({
            '-n': name,
            '-r': new_name
        })
        const { stdout, stderr } = await exec(`avdmanager move avd ${options}`)
    },

    async stopAll() {
        const { stdout, stderr } = await exec(`adb devices | grep emulator | cut -f1 | while read line; do adb -s $line emu kill; done`)
        // kill multiinstance lock
        let emulators = await this.emulatorsList()
        for(let i=0;i<emulators.length;i++) {
            let name = emulators[i]
            let kill = await exec(`rm -rf ~/.android/avd/${name}.avd/multiinstance.lock`)
            kill = await exec(`rm -rf ~/.android/avd/${name}.avd/hardware-qemu.ini.lock`)
        }
    },

    startEmulator(name, start_options, callbacks) {
        let options = [
            '-noaudio',
            ...start_options
        ].join(' ')
        let cmd = `$ANDROID_HOME/emulator/emulator -avd ${name} ${options}`
        console.log(cmd)
        const process = ChildProcess.spawn(cmd, {shell:true})
        if('exit' in callbacks)
            process.on('exit', callbacks.exit)
        if('output' in callbacks)
            process.stdout.on('data', callbacks.output)
        if('error' in callbacks)
            process.on('error', callbacks.error)
        return process
    },

    async installWhatsapp(name) {
        let dir = `${process.cwd()}/${__dirname}`
        try {
            const { stdout, stderr } = await exec(`adb install ${dir}/WhatsApp.apk`)
        } catch(e) {
            return e.message
        }
        return 'success'
    },

    async installNordVPN(name) {
        let dir = `${process.cwd()}/${__dirname}`
        try {
            const { stdout, stderr } = await exec(`adb install ${dir}/NordVPN.apk`)
        } catch(e) {
            return e.message
        }
        return 'success'
    },

    hasTools() {
        return which('emulator') != null
    },

    testChildProcess() {

    },

    killPort(port) {
        exec(`adb -s emulator-${port} emu kill`)
    }

}

export default Terminal
