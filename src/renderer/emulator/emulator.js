
import Vue from 'vue'
import Terminal from './terminal'
import EventBus from '@/bus'


export default class Emulator {
    constructor(name, id) {
        this.name = name
        this.id = id
        this.port = 5554 + (id * 2)
        this.process = null
        this.running = false
        this.mode = null
        this.modes = {
            'normal': [
                `-port ${this.port}`
                //'-skin 411x731',
            ],
            'headless': [
                `-port ${this.port}`,
                '-no-window',
            ],
            'login': [
                `-port ${this.port}`,
                '-camera-back webcam0',
                '-camera-front webcam0'
                //'-skin 411x731',
            ]
        },
        this.state = 'waiting'
        this.state_colors = {
            running: 'primary',
            waiting: 'secondary',
            installing: 'warning'
        }
        this.event = null
    }

    async installWhatsapp() {
        this.state = 'installing'
        let message = await Terminal.installWhatsapp()
        console.log(message)
        this.state = 'running'
    }


    async installNordVPN() {
        this.state = 'installing'
        let message = await Terminal.installNordVPN()
        console.log(message)
        this.state = 'running'
    }

    stop() {
        console.log(this.process)
        if(!this.process)
            return
        Terminal.killPort(this.port)
    }

    isRunning() {
        if(!this.process)
            return false
        if(!this.running)
            return false
        return this.mode
    }

    start(mode) {
        if(!mode in this.modes)
            return
        this.mode = mode
        let self = this
        this.process = Terminal.startEmulator(this.name, this.modes[mode], {
            exit: () => {
                self.running = false
                self.state = 'waiting'
            },
            error: (dta) => {

                console.log('stderr:' + data)
            },
            output: (data) => {
                let output = String(data)
                if(output.startsWith('emulator: ERROR'))
                    alert(output)
                console.log('stdout: ' + data);
            }
        })
        this.running = true
        this.state = 'running'
        console.log(this.process)
    }

}
