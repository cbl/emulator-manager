import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/views/Home'

Vue.use(Router)

const withPrefix = (prefix, routes) =>
    routes.map( (route) => {
        route.path = prefix + route.path;
        return route;
    })

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
    ]
})
