import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { API_URL } from '@/common/config'

const ApiService = {
    init() {
        Vue.use(VueAxios, axios)
        Vue.axios.defaults.baseURL = API_URL
    },

    query(resource, params) {
        return Vue.axios
            .get(resource, params)
            .catch((error) => {
                throw new Error(`ApiService ${error}`)
            })
    },
    get(resource, id = '') {
        return Vue.axios
            .get(`${resource}/${id}`)
            .catch((error) => {
                throw new Error(`ApiService ${error}`)
            })
    },
    post(resource, data) {
        return Vue.axios
            .post(`${resource}`, data)
    },
    put(resource, data) {
        return Vue.axios
            .put(`${resource}`, data)
    },
    delete(resource, id = ''){
        return Vue.axios
            .delete(`${resource}/${id}`)
            .catch((error) => {
                throw new Error(`ApiService ${error}`)
            })
    }

}

export default ApiService
