import Vue from 'vue'

import Tabs from 'vue-tabs-component';
import Notifications from 'vue-notification'
import VModal from 'vue-js-modal'
import EventBus from '@/bus'

// fontawesome
import { FontAwesomeIcon } from '\@fortawesome/vue-fontawesome';
import fasolid from '\@fortawesome/fontawesome-free-solid';

import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'

const ComponentService = {
    init() {
        this.use();
        this.components();
        this.globals()
        this.helpers()
    },
    use() {
        Vue.use(VModal)
        Vue.use(Tabs)
        Vue.use(Notifications)
    },
    components() {
        Vue.component('fa', FontAwesomeIcon)
    },
    globals() {
        Vue.prototype.$bus = EventBus;
    },
    helpers() {
        // Global Registration of Base Components with Webpack
        // https://vuejs.org/v2/guide/components-registration.html#Automatic-Global-Registration-of-Base-Components
        const requireComponent = require.context(
            './../components', // The relative path of the components folder
            false, // Whether or not to look in subfolders
            /Base[A-Z]\w+\.(vue|js)$/ // The regular expression used to match base component filenames
        )

        requireComponent.keys().forEach(fileName => {
            // Get component config
            const componentConfig = requireComponent(fileName)

            // Get PascalCase name of component
            const componentName = upperFirst(
                camelCase(
                    // Strip the leading `./` and extension from the filename
                    fileName.replace(/^\.\/(.*)\.\w+$/, '$1')
                )
            ) // Register component globally
            Vue.component(
                componentName,
                // Look for the component options on `.default`, which will
                // exist if the component was exported with `export default`,
                // otherwise fall back to module's root.
                componentConfig.default || componentConfig
            )
        })
    }
}

export default ComponentService
